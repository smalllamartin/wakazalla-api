var mongoose = require('../db.js').mongoose;


var natureSchema = mongoose.Schema({
	id: Number,
	name: String
});
var Nature =  mongoose.model('Nature', natureSchema);

exports.Nature = Nature;