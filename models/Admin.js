var mongoose = require('../db.js').mongoose;

var adminSchema = mongoose.Schema({
	name: String
});

var Admin = mongoose.model('Admin', adminSchema);

exports.Admin  = Admin;