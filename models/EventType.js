var mongoose = require('../db.js').mongoose;


var eventTypeSchema = mongoose.Schema({
	id: Number,
	name: String
});
var EventType =  mongoose.model('EventType', eventTypeSchema);

exports.EventType = EventType;