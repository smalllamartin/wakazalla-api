var mongoose = require('../db.js').mongoose;


var categorySchema = mongoose.Schema({
	id: Number,
	name: String
});
var Category =  mongoose.model('Category', categorySchema);

exports.Category = Category;