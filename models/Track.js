var mongoose = require('../db.js').mongoose;


var trackSchema = mongoose.Schema({
	id: 		Number,
	name: 		String,
	duration: 	String,
	track: 		String,
	picture:    String,
	genreName: 	String, 		// From Genre
	artistName: String 			// From Artist
});
var Track =  mongoose.model('Track', trackSchema);

exports.Track = Track;
