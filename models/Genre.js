var mongoose = require('../db.js').mongoose;


var genreSchema = mongoose.Schema({
	id: Number,
	name: String
});
var Genre =  mongoose.model('Genre', genreSchema);

exports.Genre = Genre;