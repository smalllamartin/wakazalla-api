var mongoose = require('../db.js').mongoose;


var artistSchema = mongoose.Schema({
	id: Number,
	name: String,
	picture: String,
	genreName: String
});
var Artist =  mongoose.model('Artist', artistSchema);

exports.Artist = Artist;