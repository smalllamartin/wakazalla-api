var mongoose = require('../db.js').mongoose;

var userSchema = mongoose.Schema({
	name: String
});
var User = mongoose.model('User', userSchema);

exports.User   = User;