var mongoose = require('../db.js').mongoose;


var eventSchema = mongoose.Schema({
	id: Number,
	name: String,
	place: String,
	date: Date,
	details: String,
	eventTypeName: String,	// from EventType
	artistName: String 		// from Artist
});

var Event = mongoose.model('Event', eventSchema);

exports.Event   = Event;

