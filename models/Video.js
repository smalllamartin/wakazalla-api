var mongoose = require('../db.js').mongoose;

var videoSchema = mongoose.Schema({
	id: 			Number,
	name: 			String,
	releaseDate: 	Date,
	video: 			String,
	picture: 		String,
	natureName: 	String,		// From Nature
	categoryName:	String,		// From Category
	genreName:  	String,		// From Genre
	artistName: 	String,		// From Artist
});

var Video = mongoose.model('Video', videoSchema);

exports.Video  = Video;