var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/openclassrooms');

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function () {
	console.log('Connected ok');
});

exports.mongoose = mongoose;





