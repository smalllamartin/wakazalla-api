var express		= require('express');
var router		= express.Router();
var bodyParser  = require('body-parser');

var Artist		= require('../models/Artist.js').Artist;
var Video		= require('../models/Video.js').Video;
var Track		= require('../models/Track.js').Track;
var Event		= require('../models/Event.js').Event;
var Admin		= require('../models/Admin.js').Admin;
var User		= require('../models/User.js').User;
var Category	= require('../models/Category.js').Category;
var Genre		= require('../models/Genre.js').Genre;
var Nature		= require('../models/Nature.js').Nature;
var EventType	= require('../models/EventType.js').EventType;


router.get('/', function(req, res, next) {	
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		res.render('Admin', { title: 'Page Admin' });
	}else{
		res.redirect('/admins/login');
	}
})
.get('/login', function (req, res, next){
	res.render('login', { title: 'Login page'});
})
.post('/login', function (req, res, next){
	if(req.body.username == 'toto' &&  req.body.password == 'titi'){
		req.session.pass = 'accepter';
		res.redirect('/admins');
	}else{
		res.redirect('/admins/login');   
	}
})
.get('/artists', function(req, res, next){	
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		Artist.find(function(err, artists){
			if(err){
				console.log(err);
				console.log("Les artistes ne sont pas disponibles");
				res.redirect('/admins');
			}else{
				console.log(artists);
				res.render('artists', { title: 'Page all Artists', artists: artists});		
			}
		});
	}else{
		res.redirect('/admins/login');
	}						
})
.get('/artists/:id', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		console.log(req.params.id);
		var query = Artist.findOne({ 'id': req.params.id }); 
		query.exec(function (err, artist){
			if(artist == null){
				console.log(err);
				res.redirect('/admins/artists');
			}else{
				console.log(artist);
				res.render('artist', { title: 'Page Single Artist', artist: artist});
			}
		});
	}else{
		res.redirect('/admins/login');
	}
})
.post('/artists', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var artist = new Artist({
			id: 		req.body.id,
			name: 		req.body.name,
			picture: 	req.body.picture,
			genreName:  req.body.genreName
		});
		artist.save(function(err, artist){
			if(err){
				console.log(err);
				console.log('L artiste n a pas été enregistré');
				res.redirect('/admins/artists');
			}else{
				console.log('L artiste a bien été enregistré');
				res.redirect('/admins/artists');
			}
		});
	}else{
		res.redirect('/admins/login');
	}						
})
.get('/tracks', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		Track.find(function(err, tracks){
			if(err){
				console.log(err);
				console.log("Les tracks ne sont pas disponibles");
				res.redirect('/admins');
			}else{
				console.log(tracks);
				res.render('tracks', { title: 'Page all Tracks', tracks: tracks});		
			}
		});
	}else{
		res.redirect('/admins/login');
	}						
})
.get('/tracks/:id', function(req, res, next){	
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var query = Track.findOne({ 'id': req.params.id });
		query.exec(function (err, track){
			if(track == null){
				console.log(err);
				res.redirect('/admins/tracks');
			}else{
				console.log(track);
				res.render('track', { title: 'Page Single Track', track: track});
			}
		});
	}else{
		res.redirect('/admins/login');
	}			
})
.post('/tracks', function(req, res, next){	
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var track = new Track({
			id: 		req.body.id,
			name: 		req.body.name,
			duration: 	req.body.duration,
			track: 		req.body.track, 		// From Genre
			picture: 	req.body.picture, 
			artistName: req.body.artistName 	
		});
		track.save(function(err, track){
			if(err){
				console.log(err);
				console.log('L track n a pas été enregistré');
				res.redirect('/admins/tracks');
			}else{
				console.log('L track a bien été enregistré');
				res.redirect('/admins/tracks');
			}
		});
	}else{
		res.redirect('/admins/login');
	}				
})
.get('/videos', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		Video.find(function(err, videos){
			if(err){
				console.log(err);
				console.log("Les videos ne sont pas disponibles");
				res.redirect('/admins');
			}else{
				console.log(videos);
				res.render('videos', { title: 'Page all Videos', videos: videos});		
			}
		});
	}else{
		res.redirect('/admins/login');
	}						
})
.get('/videos/:id', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var query = Video.findOne({ 'id': req.params.id });
		query.exec(function (err, video){
			if(video == null){
				console.log(err);
				res.redirect('/admins/videos');
			}else{
				console.log(video);
				res.render('video', { title: 'Page Single Video', video: video});
			}
		});
	}else{
		res.redirect('/admins/login');
	}				
})
.post('/videos', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var video = new Video({
			id: 			req.body.id,
			name: 			req.body.name,
			releaseDate: 	req.body.releaseDate,
			video: 			req.body.video,
			picture: 		req.body.picture,
			natureName: 	req.body.natureName,		// From Nature
			categoryName:	req.body.categoryName,		// From Category
			genreName:  	req.body.genreName,			// From Genre
			artistName: 	req.body.artistName,		// From Artist
		});
		video.save(function(err, video){
			if(err){
				console.log(err);
				console.log('L video n a pas été enregistré');
				res.redirect('/admins/videos');
			}else{
				console.log('L video a bien été enregistré');
				res.redirect('/admins/videos');
			}
		});
	}else{
		res.redirect('/admins/login');
	}					
})
.get('/events', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		Event.find(function(err, events){
			if(err){
				console.log(err);
				console.log("Les events ne sont pas disponibles");
				res.redirect('/admins');
			}else{
				console.log(events);
				res.render('events', { title: 'Page all Events', events: events});		
			}
		});
	}else{
		res.redirect('/admins/login');
	}						
})
.get('/events/:id', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var query = Event.findOne({ 'id': req.params.id });
		query.exec(function (err, event){
			if(event == null){
				console.log(err);
				res.redirect('/admins/events');
			}else{
				console.log(event);
				res.render('event', { title: 'Page Single Event', event: event});
			}
		});
	}else{
		res.redirect('/admins/login');
	}				
})
.post('/events', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var event = new Event({
			id: 			req.body.id,
			name: 			req.body.name,
			place: 			req.body.place,
			date: 			req.body.date,
			details: 		req.body.details,
		eventTypeName: 	req.body.eventTypeName,		// from EventType
		artistName: 	req.body.artistName 		// from Artist
	});
		event.save(function(err, event){
			if(err){
				console.log(err);
				console.log('L event n a pas été enregistré');
				res.redirect('/admins/events');
			}else{
				console.log('L event a bien été enregistré');
				res.redirect('/admins/events');
			}
		}); 
	}else{
		res.redirect('/admins/login');
	}				
})
.get('/categorys', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		Category.find(function(err, categorys){
			if(err){
				console.log(err);
				console.log("Les categorys ne sont pas disponibles");
				res.redirect('/admins');
			}else{
				console.log(categorys);
				res.render('categorys', { title: 'Page all Categorys', categorys: categorys});		
			}
		});
	}else{
		res.redirect('/admins/login');
	}						
})
.get('/categorys/:id', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var query = Category.findOne({ 'id': req.params.id });
		query.exec(function (err, category){
			if(category == null){
				console.log(err);
				res.redirect('/admins/categorys');
			}else{
				console.log(category);
				res.render('category', { title: 'Page Single Category', category: category});
			}
		});
	}else{
		res.redirect('/admins/login');
	}			
})
.post('/categorys', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var category = new Category({
			id:   req.body.id,
			name: req.body.name
		});
		category.save(function(err, category){
			if(err){
				console.log(err);
				console.log('L category n a pas été enregistré');
				res.redirect('/admins/categorys');
			}else{
				console.log('L category a bien été enregistré');
				res.redirect('/admins/categorys');
			}
		});
	}else{
		res.redirect('/admins/login');
	}				
})
.get('/natures', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		Nature.find(function(err, natures){
			if(err){
				console.log(err);
				console.log("Les natures ne sont pas disponibles");
				res.redirect('/admins');
			}else{
				console.log(natures);
				res.render('natures', { title: 'Page all Natures', natures: natures});		
			}
		});
	}else{
		res.redirect('/admins/login');
	}						
})
.get('/natures/:id', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var query = Nature.findOne({ 'id': req.params.id });
		query.exec(function (err, nature){
			if(nature == null){
				console.log(err);
				res.redirect('/admins/natures');
			}else{
				console.log(nature);
				res.render('nature', { title: 'Page Single Nature', nature: nature});
			}
		});
	}else{
		res.redirect('/admins/login');
	}				
})
.post('/natures', function(req, res, next){	
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var nature = new Nature({
			id:   req.body.id,
			name: req.body.name
		});
		nature.save(function(err, nature){
			if(err){
				console.log(err);
				console.log('L nature n a pas été enregistré');
				res.redirect('/admins/natures');
			}else{
				console.log('L nature a bien été enregistré');
				res.redirect('/admins/natures');
			}
		});
	}else{
		res.redirect('/admins/login');
	}				
})
.get('/genres', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		Genre.find(function(err, genres){
			if(err){
				console.log(err);
				console.log("Les genres ne sont pas disponibles");
				res.redirect('/admins');
			}else{
				console.log(genres);
				res.render('genres', { title: 'Page all Genres', genres: genres});		
			}
		});
	}else{
		res.redirect('/admins/login');
	}						
})
.get('/genres/:id', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var query = Genre.findOne({ 'id': req.params.id });
		query.exec(function (err, genre){
			if(genre == null){
				console.log(err);
				res.redirect('/admins/genres');
			}else{
				console.log(genre);
				res.render('genre', { title: 'Page Single Genre', genre: genre});
			}
		});
	}else{
		res.redirect('/admins/login');
	}				
})
.post('/genres', function(req, res, next){	
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var genre = new Genre({
			id:   req.body.id,
			name: req.body.name
		});
		genre.save(function(err, genre){
			if(err){
				console.log(err);
				console.log('L genre n a pas été enregistré');
				res.redirect('/admins/genres');
			}else{
				console.log('L genre a bien été enregistré');
				res.redirect('/admins/genres');
			}
		});
	}else{
		res.redirect('/admins/login');
	}				
})
.get('/eventTypes', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		EventType.find(function(err, eventTypes){
			if(err){
				console.log(err);
				console.log("Les eventTypes ne sont pas disponibles");
				res.redirect('/admins');
			}else{
				console.log(eventTypes);
				res.render('eventTypes', { title: 'Page all EventTypes', eventTypes: eventTypes});		
			}
		});
	}else{
		res.redirect('/admins/login');
	}						
})
.get('/eventTypes/:id', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var query = EventType.findOne({ 'id': req.params.id });
		query.exec(function (err, eventType){
			if(eventType == null){
				console.log(err);
				res.redirect('/admins/eventTypes');
			}else{
				console.log(eventType);
				res.render('eventType', { title: 'Page Single EventType', eventType: eventType});
			}
		});
	}else{
		res.redirect('/admins/login');
	}				
})
.post('/eventTypes', function(req, res, next){
	if(typeof req.session.pass != "undefined" &&  req.session.pass == 'accepter' ){
		var eventType = new EventType({
			id:   req.body.id,
			name: req.body.name
		});
		eventType.save(function(err, eventType){
			if(err){
				console.log(err);
				console.log('L eventType n a pas été enregistré');
				res.redirect('/admins/eventTypes');
			}else{
				console.log('L eventType a bien été enregistré');
				res.redirect('/admins/eventTypes');
			}
		});
	}else{
		res.redirect('/admins/login');
	}					
});

module.exports = router;
