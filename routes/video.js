var express 	= require('express');
var router 		= express.Router();
var Video = require('../models/Video.js').Video;


/* GET home page. */
router.get('/', function(req, res, next) {
	Video.find(function(err, videos){
		if(err){
			console.log(err);
		}else{
			console.log(videos);
			res.json({ videos: videos })
		}
	})
})
.get('/:id', function(req, res, next) {
	var query = Video.findOne({ 'id': req.params.id });
	query.exec(function (err, video){
		if(video == null){
			console.log(err);
			res.redirect('/videos');
		}else{
			console.log(video);
			res.json({ video: video })
		}
	});
});

module.exports = router;
