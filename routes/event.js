var express 	= require('express');
var router 		= express.Router();
var Event = require('../models/Event.js').Event;


/* GET home page. */
router.get('/', function(req, res, next) {
	Event.find(function(err, events){
		if(err){
			console.log(err);
		}else{
			console.log(events);
			res.json({ events: events })
		}
	})
})
.get('/:id', function(req, res, next) {
	var query = Event.findOne({ 'id': req.params.id });
	query.exec(function (err, event){
		if(event == null){
			console.log(err);
			res.redirect('/events');
		}else{
			console.log(event);
			res.json({ event: event })
		}
	});
});

module.exports = router;
