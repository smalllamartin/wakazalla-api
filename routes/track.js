var express 	= require('express');
var router 		= express.Router();
var Track = require('../models/Track.js').Track;


/* GET home page. */
router.get('/', function(req, res, next) {
	Track.find(function(err, tracks){
		if(err){
			console.log(err);
		}else{
			console.log(tracks);
			res.json({ tracks: tracks })
		}
	})
})
.get('/:id', function(req, res, next) {
	var query = Track.findOne({ 'id': req.params.id });
	query.exec(function (err, track){
		if(track == null){
			console.log(err);
			res.redirect('/tracks');
		}else{
			console.log(track);
			res.json({ track: track })
		}
	});
});

module.exports = router;
