var express 	= require('express');
var router 		= express.Router();
var Nature = require('../models/Nature.js').Nature;


/* GET home page. */
router.get('/', function(req, res, next) {
	Nature.find(function(err, natures){
		if(err){
			console.log(err);
		}else{
			console.log(natures);
			res.json({ natures: natures })
		}
	})
})
.get('/:id', function(req, res, next) {
	var query = Nature.findOne({ 'id': req.params.id });
	query.exec(function (err, nature){
		if(nature == null){
			console.log(err);
			res.redirect('/natures');
		}else{
			console.log(nature);
			res.json({ nature: nature })
		}
	});
});

module.exports = router;
