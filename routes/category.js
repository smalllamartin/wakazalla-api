var express 	= require('express');
var router 		= express.Router();
var Category = require('../models/Category.js').Category;


/* GET home page. */
router.get('/', function(req, res, next) {
	Category.find(function(err, categorys){
		if(err){
			console.log(err);
		}else{
			console.log(categorys);
			res.json({ categorys: categorys })
		}
	})
})
.get('/:id', function(req, res, next) {
	var query = Category.findOne({ 'id': req.params.id });
	query.exec(function (err, category){
		if(category == null){
			console.log(err);
			res.redirect('/categorys');
		}else{
			console.log(category);
			res.json({ category: category })
		}
	});
});

module.exports = router;
