var express 	= require('express');
var router 		= express.Router();
var EventType = require('../models/EventType.js').EventType;


/* GET home page. */
router.get('/', function(req, res, next) {
	EventType.find(function(err, eventTypes){
		if(err){
			console.log(err);
		}else{
			console.log(eventTypes);
			res.json({ eventTypes: eventTypes })
		}
	})
})
.get('/:id', function(req, res, next) {
	var query = EventType.findOne({ 'id': req.params.id });
	query.exec(function (err, eventType){
		if(eventType == null){
			console.log(err);
			res.redirect('/eventTypes');
		}else{
			console.log(eventType);
			res.json({ eventType: eventType })
		}
	});
});

module.exports = router;
