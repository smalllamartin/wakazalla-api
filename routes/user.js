var express 	= require('express');
var router 		= express.Router();
var User = require('../models/User.js').User;


/* GET home page. */
router.get('/', function(req, res, next) {
	User.find(function(err, users){
		if(err){
			console.log(err);
		}else{
			console.log(users);
			res.json({ users: users })
		}
	})
})
.get('/:id', function(req, res, next) {
	var query = User.findOne({ 'id': req.params.id });
	query.exec(function (err, user){
		if(user == null){
			console.log(err);
			res.redirect('/users');
		}else{
			console.log(user);
			res.json({ user: user })
		}
	});
});

module.exports = router;
