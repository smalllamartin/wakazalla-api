var express 	= require('express');
var router 		= express.Router();
var Artist = require('../models/Artist.js').Artist;


/* GET home page. */
router.get('/', function(req, res, next) {
	Artist.find(function(err, artists){
		if(err){
			console.log(err);
		}else{
			console.log(artists);
			res.json({ artists: artists })
		}
	})
})
.get('/:id', function(req, res, next) {
	var query = Artist.findOne({ 'id': req.params.id });
	query.exec(function (err, artist){
		if(artist == null){
			console.log(err);
			res.redirect('/artists');
		}else{
			console.log(artist);
			
			res.json({ artist: artist })
		}
	});
});

module.exports = router;
