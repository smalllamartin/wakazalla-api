var express      = require('express');
var path         = require('path');
var favicon      = require('serve-favicon');
var logger       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var cors         = require('cors');
var everyauth    = require('everyauth');
var util         = require('util');
var promise      = everyauth.Promise;


var home        = require('./routes/index');
var artists     = require('./routes/artist');
var tracks      = require('./routes/track');
var videos      = require('./routes/video');
var events      = require('./routes/event');
var users       = require('./routes/user');
var admins      = require('./routes/admin');
var categorys   = require('./routes/category');
var genres      = require('./routes/genre');
var natures     = require('./routes/nature');
var eventTypes  = require('./routes/eventType');


everyauth.facebook
.appId('773394326108017')
.appSecret('ed3e310f769db0c5371cf3977f7b5fb1')
.entryPath('/auth/facebook')
.callbackPath('/auth/facebook/callback')
.handleAuthCallbackError( function (req, res) {
    // If a user denies your app, Facebook will redirect the user to
    // /auth/facebook/callback?error_reason=user_denied&error=access_denied&error_description=The+user+denied+your+request.
    // This configurable route handler defines how you want to respond to
    // that.
    // If you do not configure this, everyauth renders a default fallback
    // view notifying the user that their authentication failed and why.
  })
.findOrCreateUser( function (session, accessToken, accessTokExtra, fbUserMetadata) {
    // find or create user logic goes here
    console.log(util.inspect(fbUserMetadata));
  })
.redirectPath('http://wakazalla.com');


everyauth.twitter
.consumerKey('c5LLPkGIquHUMhp3JQFNY1mGJ')
.consumerSecret('vgRSvsiKZM6se8HYVkdAuZ3HeYJIr5vJ2nO6pmnpk5LgAeeo6M')
.callbackPath('/custom/twitter/callback/path')
.findOrCreateUser( function (session, accessToken, accessTokenSecret, twitterUserMetadata) {
    // find or create user logic goes here
    console.log(util.inspect(twitterUserMetadata));
  })
.redirectPath('/');

everyauth.google
.appId('YOUR CLIENT ID HERE')
.appSecret('YOUR CLIENT SECRET HERE')
  .scope('https://www.google.com/m8/feeds') // What you want access to
  .handleAuthCallbackError( function (req, res) {
    // If a user denies your app, Google will redirect the user to
    // /auth/facebook/callback?error=access_denied
    // This configurable route handler defines how you want to respond to
    // that.
    // If you do not configure this, everyauth renders a default fallback
    // view notifying the user that their authentication failed and why.
  })
  .findOrCreateUser( function (session, accessToken, accessTokenExtra, googleUserMetadata) {
    // find or create user logic goes here
    // Return a user or Promise that promises a user
    // Promises are created via
    //     var promise = this.Promise();
    console.log(util.inspect(googleUserMetadata));
  })
  .redirectPath('/');


  var app = express();


  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'jade');

  app.use(favicon(__dirname + '/public/favicon.ico'));
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(session({secret : "90ndsj9dfsf"}));
  app.use(everyauth.middleware());
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(cors());

  app.use('/', home);
  app.use('/artists', artists);
  app.use('/vi2os', videos);
  app.use('/tracks', tracks);
  app.use('/events', events);
  app.use('/users', users);
  app.use('/admins', admins);
  app.use('/categorys', categorys);
  app.use('/natures', natures);
  app.use('/genres', genres);
  app.use('/natures', natures);
  app.use('/eventTypes', eventTypes);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.all('/secret', function (req, res, next) {
  console.log('Accessing the secret section ...');
  next(); // pass control to the next handler
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

var server = app.listen(3000, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log('Wakazalla api listening at http://localhost:%s', host, port);

});
module.exports = app;
